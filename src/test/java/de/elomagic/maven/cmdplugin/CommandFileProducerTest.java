/*
 * CmdLine Maven Plugin
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.maven.cmdplugin;

import java.io.File;
import java.io.StringWriter;
import java.util.Collections;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CommandFileProducerTest {
    
    @Test
    public void testWriteTo1() throws Exception {
        CommandFileProducer instance = new CommandFileProducer();

        Assertions.assertThrows(NullPointerException.class, ()->instance.writeTo(null));

        StringWriter writer = new StringWriter();

        instance.setData(Collections.singletonMap(SupportedVariable.LauncherClass, "SUPER_CLASS"));
        instance.setResource("/de/elomagic/maven/cmdplugin/app.cmd");
        instance.writeTo(writer);

        Assertions.assertTrue(writer.toString().contains("CLASS_LAUNCHER=SUPER_CLASS"));
    }

    @Test
    public void testWriteTo2() throws Exception {

        File file = File.createTempFile("test", ".txt");
        file.deleteOnExit();

        FileUtils.copyToFile(getClass().getResourceAsStream("/de/elomagic/maven/cmdplugin/app.cmd"), file);

        StringWriter writer = new StringWriter();

        CommandFileProducer instance = new CommandFileProducer();
        instance.setData(Collections.singletonMap(SupportedVariable.LauncherClass, "SUPER_CLASS"));
        instance.setResource(file);
        instance.writeTo(writer);

        Assertions.assertTrue(writer.toString().contains("CLASS_LAUNCHER=SUPER_CLASS"));
    }
    
    @Test
    public void testWriteTo3() throws Exception {

        File file = File.createTempFile("test", ".txt");
        file.deleteOnExit();

        FileUtils.copyToFile(getClass().getResourceAsStream("/de/elomagic/maven/cmdplugin/app.cmd"), file);

        StringWriter writer = new StringWriter();

        CommandFileProducer instance = new CommandFileProducer();
        instance.setData(Collections.singletonMap(SupportedVariable.LauncherClass, "SUPER_CLASS"));
        instance.setResource(file.toPath());
        instance.writeTo(writer);

        Assertions.assertTrue(writer.toString().contains("CLASS_LAUNCHER=SUPER_CLASS"));
    }

}
