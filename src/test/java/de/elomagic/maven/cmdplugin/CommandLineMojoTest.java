/*
 * CmdLine Maven Plugin
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.maven.cmdplugin;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CommandLineMojoTest {

    private CommandLineMojo mojo;

    @BeforeEach
    public void before() {
        mojo = new CommandLineMojo();
    }

    @Test
    public void testBuildData() throws Exception {

        mojo.setFilename("testFilename");
        mojo.setApplicationName("testAppName");
        mojo.setLauncherClass("testClass");

        Map<SupportedVariable, String> map = mojo.buildData();

        Assertions.assertEquals("testFilename", map.get(SupportedVariable.FileName));
        Assertions.assertEquals("testAppName", map.get(SupportedVariable.ApplicationName));
        Assertions.assertEquals("testClass", map.get(SupportedVariable.LauncherClass));

    }

    @Test
    public void testBuildFilename() throws Exception {

        mojo.setFilename("filename");

        Assertions.assertEquals("filename.sh", mojo.buildFilename(TargetResource.Bash));
        Assertions.assertEquals("filename.cmd", mojo.buildFilename(TargetResource.Command));
        Assertions.assertEquals("log4j2.xml", mojo.buildFilename(TargetResource.Log4J2));

    }

    @Test
    public void testLog4JTemplate() throws Exception {

        File file = File.createTempFile("_test_temp", "xml");
        file.deleteOnExit();
        File parent = file.getParentFile();

        mojo.setApplicationName("APPNAME");
        mojo.setFilename("filename");
        mojo.setLauncherClass("LAUNCHER_CLAZZ");
        mojo.setOutputDirectory(parent);

        mojo.write(TargetResource.Log4J2);

        Path testFile = Paths.get(parent.getAbsolutePath(), "conf", "log4j2.xml");
        testFile.toFile().deleteOnExit();

        Assertions.assertTrue(Files.exists(testFile));
        
        String content = FileUtils.readFileToString(testFile.toFile(), "utf-8");
        
        Assertions.assertTrue(content.contains("fileName=\"${env:APP_HOME}/logs/APPNAME.log\""));
        Assertions.assertTrue(content.contains("filePattern=\"${env:APP_HOME}/logs/APPNAME-%d{yyyy-MM-dd}-%i.log.zip\">"));
    }

    @Test
    public void testExecute() throws Exception {

        File tmpFolder = new File(System.getProperty("java.io.tmpdir"));
        Path cmdFile = Paths.get(tmpFolder.toString(), "runner.cmd");
        Path shFile = Paths.get(tmpFolder.toString(), "runner.sh");

        Files.deleteIfExists(cmdFile);
        Files.deleteIfExists(shFile);

        mojo.setApplicationName("TEST_APP_NAME");
        mojo.setFilename("runner");
        mojo.setLauncherClass("a.b.c.Run");
        mojo.setTargetSystems("cmd");
        mojo.setOutputDirectory(tmpFolder);

        mojo.execute();

        Assertions.assertFalse(Files.exists(shFile));

        String content = FileUtils.readFileToString(cmdFile.toFile(), "utf-8");
        Files.deleteIfExists(cmdFile);
        Files.deleteIfExists(shFile);

        Assertions.assertTrue(content.contains("CLASS_LAUNCHER=a.b.c.Run"));
    }

}
