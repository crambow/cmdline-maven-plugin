/*
 * CmdLine Maven Plugin
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.maven.cmdplugin;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.management.RuntimeErrorException;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 *
 */
@Mojo(name = "create", defaultPhase = LifecyclePhase.PREPARE_PACKAGE)
public class CommandLineMojo extends AbstractMojo {

    /**
     * Location of the file.
     */
    @Parameter(required = false, defaultValue = "${project.build.directory}")
    private File outputDirectory;
    @Parameter(required = true)
    private String launcherClass;
    @Parameter(required = false, defaultValue = "cmd,sh")
    private String targetSystems;
    @Parameter(required = false, defaultValue = "${project.build.finalName}")
    private String filename;
    @Parameter(required = false, defaultValue = "${project.name}")
    private String applicationName;
    @Parameter(required = false)
    private Map<TargetResource, String> alternatives = new HashMap();

    public void setOutputDirectory(File outputDirectory) {
        this.outputDirectory = outputDirectory;
    }

    public void setLauncherClass(String launcherClass) {
        this.launcherClass = launcherClass;
    }

    public void setTargetSystems(String targetSystems) {
        this.targetSystems = targetSystems;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    @Override
    public void execute() throws MojoExecutionException {
        try {
            Arrays.stream(targetSystems.split(","))
                    .map(s->TargetResource.valueByKey(s))
                    .forEach(r->write(r));

            write(TargetResource.Log4J2);
        } catch(RuntimeException ex) {
            throw new MojoExecutionException(ex.getMessage(), ex);
        }
    }

    void write(TargetResource targetResource) throws RuntimeErrorException {
        try {
            getLog().debug("Writing file (targetResource=" + targetResource + ";"
                                   + "launcherClass=" + launcherClass + ";"
                                   + "outputDirectory=" + outputDirectory + ";"
                                   + "filename=" + filename + ")");

            String fn = buildFilename(targetResource);

            Path outputFile = outputDirectory
                    .toPath()
                    .resolve(targetResource.getRelativePath())
                    .resolve(fn);

            if(Files.notExists(outputFile.getParent())) {
                Files.createDirectories(outputFile.getParent());
            }

            Map<TargetResource, Path> pathAlternatives = buildPathAlternatives();
            Object resource = pathAlternatives.containsKey(targetResource) ? pathAlternatives.get(targetResource) : targetResource.getResource();

            CommandFileProducer cfp = new CommandFileProducer()
                    .setData(buildData())
                    .setResource(resource);

            getLog().info("Writing file " + outputFile);

            try (Writer writer = Files.newBufferedWriter(outputFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE)) {
                cfp.writeTo(writer);
            }
        } catch(IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    Map<SupportedVariable, String> buildData() {
        Map<SupportedVariable, String> data = new HashMap();
        data.put(SupportedVariable.ApplicationName, applicationName);
        data.put(SupportedVariable.FileName, filename);
        data.put(SupportedVariable.LauncherClass, launcherClass);

        return data;
    }

    Map<TargetResource, Path> buildPathAlternatives() {
        Map<TargetResource, Path> result = new HashMap();

        alternatives.entrySet()
                .stream()
                .forEach(item->result.put(item.getKey(), Paths.get(item.getValue())));

        return result;
    }

    String buildFilename(TargetResource targetResource) {

        String fn = targetResource.isKeepFilename() ? targetResource.getFilename() : filename;

        fn += targetResource.getExtension();

        return fn;

    }
}
