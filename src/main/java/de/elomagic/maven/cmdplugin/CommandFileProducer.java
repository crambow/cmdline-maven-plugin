/*
 * CmdLine Maven Plugin
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.maven.cmdplugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class CommandFileProducer {

    private Object resource;
    private Map<SupportedVariable, String> data;

    public CommandFileProducer setResource(Object resource) {
        this.resource = resource;

        return this;
    }

    public CommandFileProducer setData(Map<SupportedVariable, String> data) {
        this.data = data == null ? Collections.emptyMap() : new HashMap(data);
        return this;
    }

    public void writeTo(Writer writer) throws IOException {
        try (InputStream in = openResource()) {
            String s = IOUtils.toString(in, StandardCharsets.UTF_8);

            if(data != null) {
                for(Entry<SupportedVariable, String> entry : data.entrySet()) {
                    s = s.replace(entry.getKey().getName(), entry.getValue());
                }
            }

            writer.write(s);
        }
    }

    private InputStream openResource() throws IOException {
        if(Path.class.isAssignableFrom(resource.getClass())) {
            Path path = (Path)resource;
            return Files.newInputStream(path, StandardOpenOption.READ);
        } else if(File.class.isAssignableFrom(resource.getClass())) {
            File file = (File)resource;
            return FileUtils.openInputStream(file);
        } else {
            return getClass().getResourceAsStream(resource.toString());
        }
    }

}
