/*
 * CmdLine Maven Plugin
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.maven.cmdplugin;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public enum TargetResource {

    Command("cmd", "/de/elomagic/maven/cmdplugin/app.cmd", "", "app", ".cmd", false),
    Bash("sh", "/de/elomagic/maven/cmdplugin/app.sh", "", "app", ".sh", false),
    Log4J2("log4j2", "/de/elomagic/maven/cmdplugin/conf/log4j2.xml", "conf", "log4j2", ".xml", true);

    private final String key;
    private final String resource;
    private final String relativePath;
    private final String filename;
    private final String extension;
    private final boolean keepFilename;

    private TargetResource(String key, String resource, String relativePath, String filename, String extension, boolean keepFilename) {
        this.key = key;
        this.resource = resource;
        this.relativePath = relativePath;
        this.filename = filename;
        this.extension = extension;
        this.keepFilename = keepFilename;
    }

    public String getResource() {
        return resource;
    }

    public Path getRelativePath() {
        return Paths.get(relativePath);
    }

    public String getFilename() {
        return filename;
    }

    public String getExtension() {
        return extension;
    }

    public boolean isKeepFilename() {
        return keepFilename;
    }

    public static TargetResource valueByKey(final String key) {
        for(final TargetResource status : values()) {
            if(status.key.equals(key)) {
                return status;
            }
        }

        throw new IllegalArgumentException("Key \"" + key + "\" not supported. Support values are: " + Arrays.toString(values()));
    }

}
