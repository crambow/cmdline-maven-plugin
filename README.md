# Command Line Maven Plugin

[![codecov](https://codecov.io/bb/crambow/cmdline-maven-plugin/branch/master/graph/badge.svg)](https://codecov.io/bb/crambow/cmdline-maven-plugin)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Download](https://api.bintray.com/packages/caramba/maven/de.elomagic.cmdline-maven-plugin/images/download.svg) ](https://bintray.com/caramba/maven/de.elomagic.cmdline-maven-plugin/_latestVersion)

A plugin for Maven to ease up assembly common files for console applications.

## Where can I download the distribution packages?

By adding the plugin into your project POM. This will looks like this:

```xml
<plugin>
    <groupId>de.elomagic</groupId>
    <artifactId>cmdline-maven-plugin</artifactId>
    <version>latestVersion</version>
    <executions>
        <execution>                        
            <id>assembly-files</id>
            <phase>package</phase>
            <goals>
                <goal>create</goal>
            </goals>
            <configuration>
                <outputDirectory>${basedir}/target/assembly/bin</outputDirectory>     
                <launcherClass>de.elomagic.core.app.Bootstrap</launcherClass>    
                <filename>cus</filename>                   
            </configuration>
        </execution>                   
    </executions>      
</plugin>
```

## FAQ

tbd

## License

The Command Line Maven Plugin is distributed under [Apache-2.0 License](https://opensource.org/licenses/Apache-2.0)
